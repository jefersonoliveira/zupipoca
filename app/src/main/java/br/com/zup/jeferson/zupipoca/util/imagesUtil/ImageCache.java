package br.com.zup.jeferson.zupipoca.util.imagesUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by jeferson on 12/04/16.
 */
public class ImageCache {

    private final String PASTA = "zupipoca";

    public void updateImageOnCache(Bitmap image, String id, Context context){
        File path = context.getDir(PASTA, Context.MODE_PRIVATE);
        path.mkdirs();
        String filename = id + ".jpeg";
        File file = new File ( path, filename );
        if( file.exists () ){
            file.delete ();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap readImageFromCache(String id, Context context){

        File path = context.getDir(PASTA, Context.MODE_PRIVATE);
        path.mkdirs();
        String filename = id + ".jpeg";
        File file = new File ( path, filename );
        if( file.exists () ){
            return BitmapFactory.decodeFile(
                    path + "/" + filename,
                new BitmapFactory.Options()
            );
        }

        return null;
    }

}
