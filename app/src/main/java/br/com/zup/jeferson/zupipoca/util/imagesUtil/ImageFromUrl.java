package br.com.zup.jeferson.zupipoca.util.imagesUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import br.com.zup.jeferson.zupipoca.util.request.DownloadImage;

/**
 * Created by jeferson on 27/08/15.
 */
public class ImageFromUrl {

    public static void get(final Context c, final String urlImagem, final ImageView imagem, final String idImagem, final Boolean isUpdateView) {
        AsyncTask<String, Void, Bitmap> asyncTask = new AsyncTask<String, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(String... params) {
                DownloadImage downloadImage = new DownloadImage();
                return downloadImage.getImageFromUrl(urlImagem, idImagem, c);
            }
            protected void onPostExecute(Bitmap bitmap) {
                if(bitmap != null && isUpdateView){
                    ImageViewChange.animate(c, imagem, bitmap);
                }
            }

        };
        asyncTask.execute();
    }

}
