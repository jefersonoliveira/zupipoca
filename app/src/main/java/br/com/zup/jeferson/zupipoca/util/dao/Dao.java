package br.com.zup.jeferson.zupipoca.util.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.zup.jeferson.zupipoca.entidades.Filme;

/**
 * Created by jeferson on 12/04/16.
 */
public class Dao {

    public static final String TABELA_FILMES = "tabelaFilmes";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String YEAR = "year";
    public static final String RATED = "rated";
    public static final String RELEASED = "released";
    public static final String RUNTIME = "runtime";
    public static final String GENRE = "genre";
    public static final String DIRECTOR = "director";
    public static final String WRITER = "writer";
    public static final String ACTORS = "actors";
    public static final String PLOT = "plot";
    public static final String LANGUAGE = "language";
    public static final String COUNTRY = "country";
    public static final String AWARDS = "awards";
    public static final String POSTER = "poster";
    public static final String IMDBID = "imdbID";
    public static final String ASSISTIDO = "assistido";

    public static final String CREATE_TABELA_FILME = "CREATE TABLE IF NOT EXISTS "
            + TABELA_FILMES
            + " ("
            + ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TITLE
            + " TEXT, "
            + YEAR
            + " TEXT, "
            + RATED
            + " TEXT, "
            + RELEASED
            + " TEXT, "
            + RUNTIME
            + " TEXT, "
            + GENRE
            + " TEXT, "
            + DIRECTOR
            + " TEXT, "
            + WRITER
            + " TEXT, "
            + ACTORS
            + " TEXT, "
            + PLOT
            + " TEXT, "
            + LANGUAGE
            + " TEXT, "
            + COUNTRY
            + " TEXT, "
            + AWARDS
            + " TEXT, "
            + POSTER
            + " TEXT, "
            + IMDBID
            + " TEXT, "
            + ASSISTIDO
            + " TEXT); ";

    public static final String DELETE_TABELA_FILME = "DROP TABLE IF EXISTS "
            + TABELA_FILMES;

    private SQLiteDatabase dataBase = null;
    private static Dao instance;
    private static Context context;

    public static Dao getInstance(Context context) {
        if (instance == null){
            instance = new Dao(context);
        }
        return instance;
    }

    private Dao(Context contextParameter) {
        PersistenceHelper persistenceHelper = PersistenceHelper
                .getInstance(contextParameter);
        dataBase = persistenceHelper.getWritableDatabase();
        context = contextParameter;
    }

    public  void salvarFilme(Filme filme){
        ContentValues values = contentValuesFilme(filme);
        dataBase.insert(TABELA_FILMES, null, values);
    }

    public void deletarFilme(String idFilme) {
        dataBase.delete(TABELA_FILMES, ID + " = " + idFilme, null);
    }

    public Integer marcaFilmeComoAssistido(String idFilme ){
        ContentValues values = new ContentValues();
        values.put(ASSISTIDO, "1");
        return dataBase.update(TABELA_FILMES, values, ID+" = "+idFilme, null);
    }

    public Integer marcaFilmeComoNaoAssistido(String idFilme ){
        ContentValues values = new ContentValues();
        values.put(ASSISTIDO, "0");
        return dataBase.update(TABELA_FILMES, values, ID+" = "+idFilme, null);
    }

    public List<Filme> listarFilmes(){
        String queryReturnAll = "SELECT * FROM " + TABELA_FILMES;
        Cursor cursor = dataBase.rawQuery( queryReturnAll, null);
        List<Filme> filmes = construirFilmesPorCursor(cursor);
        return filmes;
    }

    public Filme listarFilmePorId(String idFilme){
        String queryReturnAll = "SELECT * FROM " + TABELA_FILMES + " WHERE " + ID + " = " + idFilme;
        Cursor cursor = dataBase.rawQuery( queryReturnAll, null);
        List<Filme> filmes = construirFilmesPorCursor(cursor);
        return filmes.get(0);
    }

    public Integer verificaSeJaExisteFilme(String imdbId){
        String queryReturnAll = "SELECT * FROM " + TABELA_FILMES + " WHERE " + IMDBID + " = '" + imdbId + "'";
        Cursor cursor = dataBase.rawQuery( queryReturnAll, null);
        List<Filme> filmes = construirFilmesPorCursor(cursor);
        return filmes.size();
    }

    private ContentValues contentValuesFilme(Filme filme) {
        ContentValues values = new ContentValues();
        values.put(TITLE,filme.getTitle());
        values.put(YEAR,filme.getYear());
        values.put(RATED,filme.getRated());
        values.put(RELEASED,filme.getReleased());
        values.put(RUNTIME,filme.getRuntime());
        values.put(GENRE,filme.getGenre());
        values.put(DIRECTOR,filme.getDirector());
        values.put(WRITER,filme.getWriter());
        values.put(ACTORS,filme.getActors());
        values.put(PLOT,filme.getPlot());
        values.put(LANGUAGE,filme.getLanguage());
        values.put(COUNTRY,filme.getCountry());
        values.put(AWARDS,filme.getAwards());
        values.put(POSTER,filme.getPoster());
        values.put(IMDBID,filme.getImdbID());
        values.put(ASSISTIDO,"0");
        return values;
    }

    private List<Filme> construirFilmesPorCursor( Cursor cursor ){
        List<Filme> filmes = new ArrayList<>();
        if( cursor == null ){
            return filmes;
        }
        try{
            if( cursor.moveToFirst() ){
                do {
                    int indexId = cursor.getColumnIndex(ID);
                    int indexTitle = cursor.getColumnIndex(TITLE);
                    int indexYear = cursor.getColumnIndex(YEAR);
                    int indexRated = cursor.getColumnIndex(RATED);
                    int indexReleased = cursor.getColumnIndex(RELEASED);
                    int indexRuntime = cursor.getColumnIndex(RUNTIME);
                    int indexGenre = cursor.getColumnIndex(GENRE);
                    int indexDirector = cursor.getColumnIndex(DIRECTOR);
                    int indexWriter = cursor.getColumnIndex(WRITER);
                    int indexActors = cursor.getColumnIndex(ACTORS);
                    int indexPlot = cursor.getColumnIndex(PLOT);
                    int indexLanguage = cursor.getColumnIndex(LANGUAGE);
                    int indexCountry = cursor.getColumnIndex(COUNTRY);
                    int indexAwards = cursor.getColumnIndex(AWARDS);
                    int indexPoster = cursor.getColumnIndex(POSTER);
                    int indexImdbID = cursor.getColumnIndex(IMDBID);
                    int indexAssistido =  cursor.getColumnIndex(ASSISTIDO);

                    String id = cursor.getString(indexId);
                    String title = cursor.getString(indexTitle);
                    String year = cursor.getString(indexYear);
                    String rated = cursor.getString(indexRated);
                    String released = cursor.getString(indexReleased);
                    String runtime = cursor.getString(indexRuntime);
                    String genre = cursor.getString(indexGenre);
                    String director = cursor.getString(indexDirector);
                    String writer = cursor.getString(indexWriter);
                    String actors = cursor.getString(indexActors);
                    String plot = cursor.getString(indexPlot);
                    String language = cursor.getString(indexLanguage);
                    String country = cursor.getString(indexCountry);
                    String awards = cursor.getString(indexAwards);
                    String poster = cursor.getString(indexPoster);
                    String imdbID = cursor.getString(indexImdbID);
                    String assistido = cursor.getString(indexAssistido);

                    Filme filme = new Filme();
                    filme.setId(id);
                    filme.setTitle(title);
                    filme.setYear(year);
                    filme.setRated(rated);
                    filme.setReleased(released);
                    filme.setRuntime(runtime);
                    filme.setGenre(genre);
                    filme.setDirector(director);
                    filme.setWriter(writer);
                    filme.setActors(actors);
                    filme.setPlot(plot);
                    filme.setLanguage(language);
                    filme.setCountry(country);
                    filme.setAwards(awards);
                    filme.setPoster(poster);
                    filme.setImdbID(imdbID);
                    filme.setAssistido(assistido);

                    filmes.add(filme);
                } while ( cursor.moveToNext() );
            }
        }finally {
            cursor.close();
        }
        return filmes;
    }

}
