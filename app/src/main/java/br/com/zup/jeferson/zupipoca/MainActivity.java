package br.com.zup.jeferson.zupipoca;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.zup.jeferson.zupipoca.entidades.Filme;
import br.com.zup.jeferson.zupipoca.util.adapter.AdapterFilmes;
import br.com.zup.jeferson.zupipoca.util.dao.Dao;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    private Dao dao;
    private AdapterFilmes adapterFilmes;
    @ViewById(R.id.toolbarHeader)
    Toolbar toolbarHeader;
    @ViewById(R.id.rlProgress)
    RelativeLayout rlProgress;
    @ViewById(R.id.rlEmpty)
    RelativeLayout rlEmpty;
    @ViewById(R.id.gridView)
    GridView gridView;

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbarHeader);
        dao = Dao.getInstance(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        rlProgress.setVisibility(View.VISIBLE);
        preencheGridDeFilmes();
    }

    @Click(R.id.add)
    void add(){
        Intent it = new Intent(getApplicationContext(), AdicionarActivity_.class);
        startActivity(it);
    }

    @Background
    void preencheGridDeFilmes() {
        preencheGridDeFilmes(dao.listarFilmes());
    }

    @UiThread
    void preencheGridDeFilmes(List<Filme> filmes) {
        rlProgress.setVisibility(View.GONE);
        if(filmes.size() > 0){
            rlEmpty.setVisibility(View.GONE);
        }else{
            rlEmpty.setVisibility(View.VISIBLE);
        }
        setaAdapter(filmes);
    }

    public void setaAdapter(List<Filme> filmes){
        adapterFilmes = new AdapterFilmes(getApplicationContext(), filmes);
        gridView.setAdapter(adapterFilmes);
    }

    @ItemClick(R.id.gridView)
    void listViewCultivarItemClicked(Filme filme) {
        Intent it = new Intent(getApplicationContext(), DetalheActivity.class);
        it.putExtra("filme", filme);
        startActivity(it);
    }

}
