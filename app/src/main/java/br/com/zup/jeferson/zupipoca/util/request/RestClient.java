package br.com.zup.jeferson.zupipoca.util.request;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import br.com.zup.jeferson.zupipoca.entidades.Filme;

/**
 * Created by jeferson on 11/04/2016.
 */
@Rest(rootUrl = "http://www.omdbapi.com/",
        converters = { GsonHttpMessageConverter.class, StringHttpMessageConverter.class } )
@Accept(MediaType.APPLICATION_JSON)
public interface RestClient extends RestClientErrorHandling {

    @Get("/?t={nome}&r=json")
    Filme getFilmePorNome(String nome);

}
