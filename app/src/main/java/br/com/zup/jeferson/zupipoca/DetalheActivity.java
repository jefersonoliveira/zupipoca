package br.com.zup.jeferson.zupipoca;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.zup.jeferson.zupipoca.entidades.Filme;
import br.com.zup.jeferson.zupipoca.util.dao.Dao;
import br.com.zup.jeferson.zupipoca.util.imagesUtil.ImageCache;
import br.com.zup.jeferson.zupipoca.util.imagesUtil.ImageFromUrl;

public class DetalheActivity extends AppCompatActivity implements NestedScrollView.OnScrollChangeListener {

    private Dao dao;
    private NestedScrollView mNestedScrollView;
    private ImageView mImageView;
    private View mImageContainer;
    private View mToolbarContainer;
    private LinearLayout mContentLayout;
    private Toolbar toolbar;
    private TextView txtSinopse;
    private TextView txtElenco;
    private TextView txtDirecao;
    private TextView txtEscritor;
    private TextView txtGenero;
    private TextView txtDuracao;
    private TextView txtEstreia;
    private TextView txtPremios;
    private Button btRemover;
    private Filme filme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detalhe);

        mNestedScrollView = (NestedScrollView) findViewById(R.id.nested_scroll_view);
        mImageView = (ImageView) findViewById(R.id.image_view);
        mImageContainer = findViewById(R.id.image_container);
        mContentLayout = (LinearLayout) findViewById(R.id.content_layout);
        mToolbarContainer = findViewById(R.id.toolbar_container);
        txtSinopse = (TextView) findViewById(R.id.txtSinopse);
        txtElenco = (TextView) findViewById(R.id.txtElenco);
        txtDirecao = (TextView) findViewById(R.id.txtDirecao);
        txtEscritor = (TextView) findViewById(R.id.txtEscritor);
        txtGenero = (TextView) findViewById(R.id.txtGenero);
        txtDuracao = (TextView) findViewById(R.id.txtDuracao);
        txtEstreia = (TextView) findViewById(R.id.txtEstreia);
        txtPremios = (TextView) findViewById(R.id.txtPremios);
        btRemover = (Button) findViewById(R.id.btRemover);

        dao = Dao.getInstance(getApplicationContext());

        setupToolbar();
        setupNestedScrollView();
        setButtonRemover();
        getExtras();

    }

    private void setImageToHeightDisplaySize(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mImageView.getLayoutParams().height = size.y;
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    protected void getExtras() {
        Intent it = getIntent();
        Bundle extras = it.getExtras();
        if (extras != null) {
            filme = (Filme) extras.getSerializable("filme");
            preencheDetalhes(filme);
        }
    }

    private void preencheDetalhes(Filme filme){

        if(filme.getPoster() != null && !filme.getPoster().equals("N/A")){
            setImageToHeightDisplaySize();

            ImageCache imageCache = new ImageCache();
            Bitmap imageFromCache = imageCache.readImageFromCache( filme.getImdbID(), getApplicationContext() );

            if( imageFromCache == null ){
                ImageFromUrl.get(getApplicationContext(), filme.getPoster()
                        , mImageView, filme.getImdbID(), true);
            }else{
                mImageView.setImageBitmap(imageFromCache);
            }

        }

        getSupportActionBar().setTitle(filme.getTitle());
        txtSinopse.setText(filme.getPlot());
        txtElenco.setText(filme.getActors());
        txtDirecao.setText(filme.getDirector());
        txtEscritor.setText(filme.getWriter());
        txtGenero.setText(filme.getGenre());
        txtDuracao.setText(filme.getRuntime());
        txtEstreia.setText(filme.getReleased());
        txtPremios.setText(filme.getAwards());

    }

    private void setupNestedScrollView() {
        mNestedScrollView.setOnScrollChangeListener(this);

        ViewTreeObserver viewTreeObserver = mNestedScrollView.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    int toolbarLinearLayoutHeight = mToolbarContainer.getHeight();
                    int imageHeight = mImageView.getHeight();

                    ViewGroup.LayoutParams layoutParams = mImageContainer.getLayoutParams();
                    if (layoutParams.height != imageHeight) {
                        layoutParams.height = imageHeight;
                        mImageContainer.setLayoutParams(layoutParams);
                    }

                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) mContentLayout.getLayoutParams();
                    if (marginLayoutParams.topMargin != toolbarLinearLayoutHeight + imageHeight) {
                        marginLayoutParams.topMargin = toolbarLinearLayoutHeight + imageHeight;
                        mContentLayout.setLayoutParams(marginLayoutParams);
                    }

                    onScrollChange(mNestedScrollView, 0, 0, 0, 0);
                }
            });
        }
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        float newY = Math.max(mImageView.getHeight(), scrollY);
        ViewCompat.setTranslationY(mToolbarContainer, newY);
        ViewCompat.setTranslationY(mImageContainer, scrollY * 0.5f);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setButtonRemover(){
        btRemover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dao.deletarFilme(filme.getId());
                finish();
            }
        });
    }

}
