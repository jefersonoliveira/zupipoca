package br.com.zup.jeferson.zupipoca;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.List;

import br.com.zup.jeferson.zupipoca.entidades.Filme;
import br.com.zup.jeferson.zupipoca.util.dao.Dao;
import br.com.zup.jeferson.zupipoca.util.imagesUtil.ImageFromUrl;
import br.com.zup.jeferson.zupipoca.util.request.ErrorRest;
import br.com.zup.jeferson.zupipoca.util.request.RestClient;

@EActivity(R.layout.activity_adicionar)
public class AdicionarActivity extends AppCompatActivity {

    private Filme filmeTemp;
    private Dao dao;
    @Bean
    ErrorRest restErro;
    @RestService
    RestClient restClient;
    @ViewById(R.id.rlBuscar)
    RelativeLayout rlBuscar;
    @ViewById(R.id.edNomeFilme)
    EditText edNomeFilme;
    @ViewById(R.id.btBuscar)
    ImageButton btBuscar;
    @ViewById(R.id.rlProgress)
    RelativeLayout rlProgress;
    @ViewById(R.id.rlNadaEncontrado)
    RelativeLayout rlNadaEncontrado;
    @ViewById(R.id.rlFilmeEncontrado)
    RelativeLayout rlFilmeEncontrado;
    @ViewById(R.id.imageFilme)
    ImageView imageFilme;
    @ViewById(R.id.txtGenero)
    TextView txtGenero;
    @ViewById(R.id.txtTitulo)
    TextView txtTitulo;
    @ViewById(R.id.txtAno)
    TextView txtAno;
    @ViewById(R.id.btAdicionar)
    Button btAdicionar;
    @ViewById(R.id.rlSemConexao)
    RelativeLayout rlSemConexao;

    @AfterViews
    void afterViews() {
        onTextChangesOnSomeTextViews();
        dao = Dao.getInstance(getApplicationContext());
    }

    @Click(R.id.fechar)
    void fechar(){
        finish();
    }

    @TextChange(R.id.edNomeFilme)
    void onTextChangesOnSomeTextViews() {
        liberaOuBloqueiaBusca();
    }

    private void liberaOuBloqueiaBusca(){
        if(edNomeFilme.getText().length() > 0){
            btBuscar.setClickable(true);
        }else{
            btBuscar.setClickable(false);
        }
    }

    @Click(R.id.btBuscar)
    void btBuscarFilme(){
        hideKeyboard();
        rlBuscar.setVisibility(View.GONE);
        rlProgress.setVisibility(View.VISIBLE);
        buscarFilmePorNome(edNomeFilme.getText().toString());
    }

    private void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edNomeFilme.getWindowToken(), 0);
    }

    @Click({R.id.btVoltarBusca, R.id.btVoltarAdiciona})
    void btVoltarBusca(){
        rlNadaEncontrado.setVisibility(View.GONE);
        rlFilmeEncontrado.setVisibility(View.GONE);
        rlBuscar.setVisibility(View.VISIBLE);
    }

    @Background
    void buscarFilmePorNome(String nomeFilme) {
        buscarFilmePorNome(restClient.getFilmePorNome(nomeFilme));
    }

    @UiThread
    void buscarFilmePorNome(Filme filme) {
        rlProgress.setVisibility(View.GONE);
        if(filme != null && filme.getResponse().equals("True")){
            rlFilmeEncontrado.setVisibility(View.VISIBLE);
            preenchePreviaDeFilme(filme);
        }else if(filme != null && filme.getResponse().equals("False")){
            rlNadaEncontrado.setVisibility(View.VISIBLE);
        }else{
            rlSemConexao.setVisibility(View.VISIBLE);
        }
    }

    @AfterInject
    void afterInject() {
        restErro = ErrorRest.criaErrorRestDefault(getApplicationContext());
        restClient.setRestErrorHandler(restErro);
    }

    private void preenchePreviaDeFilme(Filme filme){
        desativaOuLiberaAdicionarSeJaExisteFilme(filme);
        txtGenero.setText(filme.getGenre());
        txtTitulo.setText(filme.getTitle());
        txtAno.setText(filme.getYear());
        imageFilme.setImageBitmap(null);
        if(filme.getPoster() != null && !filme.getPoster().equals("N/A")){
            ImageFromUrl.get(getApplication(), filme.getPoster(), imageFilme, "temp_poster", true);
        }
        filmeTemp = filme;
    }

    private void desativaOuLiberaAdicionarSeJaExisteFilme(Filme filme){
        if(dao.verificaSeJaExisteFilme(filme.getImdbID()) == 0){
            btAdicionar.setVisibility(View.VISIBLE);
        }else{
            btAdicionar.setVisibility(View.INVISIBLE);
        }
    }

    @Click(R.id.btAdicionar)
    void btAdicionar(){
        dao.salvarFilme(filmeTemp);
        finish();
    }

}
