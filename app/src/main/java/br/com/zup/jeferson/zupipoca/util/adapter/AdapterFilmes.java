package br.com.zup.jeferson.zupipoca.util.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.zup.jeferson.zupipoca.R;
import br.com.zup.jeferson.zupipoca.entidades.Filme;
import br.com.zup.jeferson.zupipoca.util.imagesUtil.ImageCache;
import br.com.zup.jeferson.zupipoca.util.imagesUtil.ImageFromUrl;

/**
 * Created by jeferson on 12/04/16.
 */
public class AdapterFilmes extends BaseAdapter {

    private final Context context;
    private List<Filme> itens;

    public AdapterFilmes(Context context, List<Filme> itens) {
        this.context = context;
        this.itens = itens;
    }

    public int getCount() {
        return itens.size();
    }

    public Filme getItem(int position) {
        return itens.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Filme item = itens.get(position);

        View gridView = inflater.inflate(R.layout.row_filme, null);;

        ImageView imageView = ((ImageView) gridView.findViewById(R.id.grid_item_image));
        TextView title = (TextView) gridView.findViewById(R.id.txtDescricao);
        title.setText(item.getTitle());

        if(item.getPoster() != null && !item.getPoster().equals("N/A")){
            ImageCache imageCache = new ImageCache();
            Bitmap imageFromCache = imageCache.readImageFromCache( item.getImdbID(), context );

            if( imageFromCache == null ){
                ImageFromUrl.get(context, item.getPoster()
                        , imageView, item.getImdbID(), true);
            }else{
                imageView.setImageBitmap(imageFromCache);
            }

            title.setVisibility(View.GONE);
        }

        gridView.setTag(item);

        return gridView;
    }

}
