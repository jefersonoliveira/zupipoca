package br.com.zup.jeferson.zupipoca.util.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Jeferson on 12/04/2016.
 */
public class PersistenceHelper extends SQLiteOpenHelper {

    public static final String NOME_BANCO =  "zupipoca";
    public static final int VERSAO =  1;

    private static PersistenceHelper instance;

    private PersistenceHelper(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    public static PersistenceHelper getInstance(Context context) {
        if(instance == null){
            instance = new PersistenceHelper(context);
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Dao.CREATE_TABELA_FILME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Dao.DELETE_TABELA_FILME);
        onCreate(db);
    }

}
