package br.com.zup.jeferson.zupipoca.util.request;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import br.com.zup.jeferson.zupipoca.util.imagesUtil.ImageCache;

/**
 * Created by jeferson on 12/03/15.
 */
public class DownloadImage {

    public Bitmap getImageFromUrl(String URL, String idImagem, Context context)
    {
        Bitmap bitmap = null;
        InputStream in = null;
        try {
            in = OpenHttpConnection(URL);
            if( in != null ) {

                BitmapFactory.Options options=new BitmapFactory.Options();
                options.inPurgeable=true;

                bitmap = BitmapFactory.decodeStream(in, null, options);

                in.close();
            }
        } catch (IOException e) {
            Log.e("IOException", e.getMessage());
        }
        if( bitmap != null && idImagem != null ){
            ImageCache imageCache = new ImageCache();
            imageCache.updateImageOnCache( bitmap, idImagem, context );
        }


        return bitmap;
    }
    protected InputStream OpenHttpConnection(String urlString)throws IOException{
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK)
            {
                in = httpConn.getInputStream();
            }
        }
        catch (Exception ex)
        {
            throw new IOException("Error connecting");
        }
        return in;
    }

}
