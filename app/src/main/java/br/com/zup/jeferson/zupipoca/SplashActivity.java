package br.com.zup.jeferson.zupipoca;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends Activity {

    @ViewById(R.id.logoSplash)
    ImageView logoSplash;

    @AfterViews
    void afterViews() {

        AlphaAnimation alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(this, R.anim.alpha);
        logoSplash.startAnimation(alphaAnimation);
        splashSleep(2);

    }

    public void splashSleep(int seconds){

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                Intent it = new Intent(getApplicationContext(), MainActivity_.class);
                startActivity(it);
                finish();

            }
        }, seconds* 1000);

    }

}