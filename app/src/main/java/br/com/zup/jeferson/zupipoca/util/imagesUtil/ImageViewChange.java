package br.com.zup.jeferson.zupipoca.util.imagesUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * Created by jeferson on 27/08/15.
 */
public class ImageViewChange {

    public static void animate(Context c, final ImageView v, final Bitmap new_image) {
        final Animation anim_in  = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        v.setImageBitmap(new_image);
        anim_in.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {}
        });
        v.startAnimation(anim_in);
    }

}
